# Service API GO

## Overview

This application combines a hexagonal architecture with gRPC to create a modular, testable, and scalable service-oriented architecture. gRPC provides efficient and language-agnostic communication, enabling seamless interactions between different services and clients.

## Key Features

* Hexagonal Architecture:
    * Clear separation of concerns for better maintainability and testability.
    * Independent domain logic, isolated from infrastructure concerns.
* gRPC:
    * High-performance, reliable communication framework.
    * Language-agnostic, supporting a variety of clients and services.
    * Bidirectional streaming for real-time communication.
* Dependency Injection:
    * Loose coupling and easier configuration management.
* Independently Testable Components:
    * Unit testing for core logic without external dependencies.
* Adaptable to Different Infrastructures:
    * Easily integrate with various databases, messaging systems, and other services.

## Project Structure
```
├── pkg                  # Global Service Utilities
│   ├── errors
│   ├── utils
│   └── logging
├── services             # Service Modules (eg. F)
│   └── ...                 # Module name : eg. funding, financing, etc
|       ├── api                 # API Entry Point
|       └── app                 # Application Logic
|           ├── ...
|           └── ports.go            # Adapter for App Function
|       ├── data                # Data Schema
|       └── datastore           # Database Access 
|           ├── ...
|           └── ports.go            # Adapter for Database Function
|       ├── test
|       ├── config.env
|       └── main.go
├── go.mod
├── go.sum
└── README.md
```


## Getting started
Clone the repository:
```
git clone https://github.com/<your-username>/<your-repo-name>.git
```


Install dependencies:
```
cd <your-repo-name>
go mod download
```


Run the application:
```
cd services/<service-name>
go run main.go
```

## Logging
- Error : 
    - Condition : Error Script, Database Connection Error
    - What to Log  : Error Description, Params and Result
- Warn
    - Condition : Data Validation
    - What to Log : Data, Query Result
- Info
    - Condition : Check Request Param, Function Call
    - What to Log : Request Param, Execution Time, Function Call Parameter and Result

## Development

## Environment Variable
This project uses environment variables for configuration. Create a `.env` file in the project root and define the following variables:

```

# Service Name
SERVICE=

# Database configuration
DB_HOST=sample
DB_PORT=sample
DB_USER=sample
DB_PASSWORD=sample
DB_DATABASE=sample
DB_SN=sample
DB_DRIVER=sample

CORE_SCHEMA=sample
ENT_SCHEMA=sample
FIN_SCHEMA=sample
CMS_SHEMA=sample
REP_SCHEMA=sample
TMP_SCHEMA=sample

# Listener
GRPC_HOST=sample
GRPC_PORT=sample

# Telemetry
SERVICENAME=sample
OTEL_COLLECTOR_URL=sample
TRACER_NAME=sample

# Minio
MINIO_ENDPOINT=sample
MINIO_SECURE=sample
MINIO_BUCKET=sample
MINIO_URL_PATH=sample
MINIO_ACCESS_KEY=sample
MINIO_SECRET_KEY=sample
```

## Coding Rules

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!
